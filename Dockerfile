FROM 100.125.0.198:20202/hwcse/as-go:1.8.5

COPY ./qqq04 /home
COPY ./conf /home/conf
RUN chmod +x /home/qqq04

CMD ["/home/qqq04"]